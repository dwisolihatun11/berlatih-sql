1. Membuat Database
    create database myshop;

2. Membuat Table di dalam Database

    Table users
        use myshop;
        create table users(
            -> id int primary key auto_increment,
            -> name varchar(255),
            -> email varchar(255),
            -> password varchar(255)
            -> );

    Table categories
        create table categories(
            -> id int primary key auto_increment,
            -> name varchar(255)
            -> );

    Table items
        create table items(
            -> id int primary key auto_increment,
            -> name varchar(255),
            -> description varchar(255),
            -> price int,
            -> stock int,
            -> category_id int,
            -> foreign key(category_id) references categories(id)
            -> );

3. Memasukan Data pada Table
    Memasukan data pada table users
        insert into users(name,email,password)values("John Doe","john@doe.com","john123"),("Jane Doe","jane@doe.com","jenita123");

    Memasukan data pada table categories
        insert into categories(name) values("gadget"),("cloth"),("men"),("women"),("branded");

    Memasukan data pada table items
        insert into items(name,description,price,stock,category_id) values("Sumsang b50","hape keren dari merek sumsang",4000000,100,1),("Uniklooh","baju keren dari brand ternama",500000,50,2),("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);


4. Mengambil Data dari Database
    a. Mengambil data users tanpa field password
        select id,name,email from users;

    b. Mengambil data items
        menampilkan data pada tabel items yang memiliki price lebih dari 1000000
            select * from items where price >1000000;
        
        menampilkan data pada tabel items yang memiliki nama serupa (like) 'sang'
            select * from items where name like '%sang%';

    d. Menampilkan data items join dengan kategori
            select items.name, items.description, items.price, items.stock, items.category_id, categories.name from items inner join categories on items.category_id = categories.id;


5. Mengubah Data dari Database
     update items set price = 2500000
        -> where name = "Sumsang b50";